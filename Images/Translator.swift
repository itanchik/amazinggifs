//
//  Translator.swift
//  Images
//
//  Created by Tanya Zhdanova on 14/06/17.
//  Copyright © 2017 zhdanovats. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift

let pageSize = 25

let URL_STRING = "http://api.giphy.com"
let API_KEY = "dc6zaTOxFJmzC"

class Translator {
    
    var limit = 0
    
    var gifSearchString = "/v1/gifs/search"

    let realm = try! Realm()

    
    func getJsonDataForPage(page: Int, searchString: String, completionHandler: @escaping (_ gifs: NSArray) -> ()) {
        
        var gifsDict = [NSDictionary]()
        
        let skip = page * pageSize
    
        let urlWithParams = URL_STRING + gifSearchString + "?q=\(searchString)&api_key=\(API_KEY)&limit=\(pageSize)&offset=\(skip)"
        var encodedUrl:NSURL!
        if let url = urlWithParams.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed){
          encodedUrl = NSURL(string: url)
        }
        let request = NSMutableURLRequest(url:encodedUrl as URL)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {data,response,error in
            if error != nil
            {
                print("error=\(String(describing: error))")
                return
            }
            
            do {
                if (try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary) != nil {
                    
                    let json = JSON(data: data!)

                    for (stringKey, subJson):(String, JSON) in json {
                        self.limit = (json["pagination"]["total_count"]).intValue

                        if stringKey == "data"{
                            
                            for var i in  (0..<subJson.count){
                                
                                for (stringKey1, subJson):(String, JSON) in subJson[i]{
                                    
                                    if stringKey1 == "images"{
                                        
                                        let key = skip + i
                                        
                                        if key >= self.limit {
                                            completionHandler(gifsDict as NSArray)
                                        }
                                        
                                        var smallPict = ""
                                        var smallPictWidth = 0.0
                                        var smallPictHeight = 0.0
                                        var largePict = ""
                                        
                                        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                                            smallPict = subJson["fixed_width"]["url"].stringValue
                                            smallPictWidth = Double(subJson["fixed_width"]["width"].floatValue)
                                            smallPictHeight = Double(subJson["fixed_width"]["height"].floatValue)
                                        }
                                        else{
                                            smallPict = subJson["fixed_width_small"]["url"].stringValue
                                            smallPictWidth = Double(subJson["fixed_width_small"]["width"].floatValue)
                                            smallPictHeight = Double(subJson["fixed_width_small"]["height"].floatValue)
                                        }
                                        largePict = subJson["original"]["url"].stringValue
                                        
                                        gifsDict.append(["id": key, "smallPict" : smallPict, "smallWidth" : smallPictWidth, "smallHeight" : smallPictHeight, "largePict" : largePict])
                                    }
                                }
                            }
                        }
                    }
                    completionHandler(gifsDict as NSArray)
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
            
        }
        
        task.resume()
    }
    
    func saveGifs(gifsDict: [NSDictionary]) {
        DispatchQueue(label: "com.zhdanovats.Images").async {
            let realm = try! Realm()
            try! realm.write {
                for gif in gifsDict {
                    realm.create(ImageItem.self, value: gif, update: true)
                }
            }
        }
    }

}
