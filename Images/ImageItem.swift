//
//  ImageItem.swift
//  Images
//
//  Created by Tanya Zhdanova on 12.06.17.
//  Copyright © 2017 zhdanovats. All rights reserved.
//

import UIKit
import RealmSwift

class ImageItem: Object {
    
    dynamic var id = 0
    dynamic var smallPict = ""
    dynamic var smallWidth = 100.0
    dynamic var smallHeight = 100.0
    dynamic var largePict = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
