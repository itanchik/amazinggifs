//
//  DetailViewController.swift
//  Images
//
//  Created by Tanya Zhdanova on 12.06.17.
//  Copyright © 2017 zhdanovats. All rights reserved.
//

import UIKit
import SDWebImage
import NVActivityIndicatorView

let ACTIVITY_INDICATOR_WIDTH = 50.0

class DetailViewController: UIViewController {
    
    @IBOutlet weak var bigImageView: UIImageView!
    
    var imageString = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.titleView = nil
        
        var activityIndicatorView: NVActivityIndicatorView
        
        activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: Double(self.view.frame.size.width/2) - ACTIVITY_INDICATOR_WIDTH/2, y:Double(self.view.frame.size.height/2) - ACTIVITY_INDICATOR_WIDTH/2, width: ACTIVITY_INDICATOR_WIDTH, height:ACTIVITY_INDICATOR_WIDTH), type: .ballScaleRippleMultiple, color: UIColor.random, padding: 0)
        activityIndicatorView.startAnimating()
        self.view.addSubview(activityIndicatorView)
        
    
        self.bigImageView.sd_setImage(with: URL(string: imageString),
                                      completed: { SDWebImageCompletionBlock in
            activityIndicatorView.stopAnimating()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
