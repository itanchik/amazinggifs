//
//  MainCollectionViewController.swift
//  Images
//
//  Created by Tanya Zhdanova on 12.06.17.
//  Copyright © 2017 zhdanovats. All rights reserved.
//

import UIKit
import SDWebImage
import RealmSwift

private let reuseIdentifier = "Cell"

class MainCollectionViewController: UICollectionViewController, UISearchBarDelegate, UICollectionViewDelegateFlowLayout {

    var searchString = ""
    
    let realm = try! Realm()
    var notificationToken: NotificationToken?
    let translator = Translator()
    
    var searchBar:UISearchBar?
    
    var queue = OperationQueue()
    
    var dataSourceForSearchResult: Results<ImageItem>? = nil
 
    let preloadMargin = 1
    var lastLoadedPage = 0
    
    @IBOutlet weak var nothingLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        dataSourceForSearchResult = getItemsFromDatabase()
 
        try! realm.write {
            self.realm.deleteAll()
        }
        
        notificationToken = realm.addNotificationBlock { [unowned self] note, realm in
            self.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.prepareUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refreashControlAction(){
        self.cancelSearching()
        self.collectionView?.reloadData()
    }

    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        if let gifs = self.dataSourceForSearchResult {
            return gifs.count
        }
        return 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:ImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ImageCollectionViewCell
        
        let nextPage: Int = Int(indexPath.item / pageSize) + 1
        let preloadIndex = nextPage * pageSize - preloadMargin
        
        if (indexPath.item >= preloadIndex && lastLoadedPage < nextPage) {
            getData(page:nextPage, searchString: searchString)
        }
        cell.smallImageView.backgroundColor = UIColor.random
        if let name = self.dataSourceForSearchResult?[indexPath.row].smallPict{
            cell.smallImageView.sd_setImage(with: URL(string: name),
                                            completed: { SDWebImageCompletionBlock in
            })
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let kWidth = self.dataSourceForSearchResult?[indexPath.row].smallWidth
        let kHeight = self.dataSourceForSearchResult?[indexPath.row].smallHeight
        return CGSize(width: CGFloat(kWidth!), height: CGFloat(kHeight!))
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchString = searchText
        try! realm.write {
            self.realm.deleteAll()
        }
        getData(page:0, searchString:searchString)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.cancelSearching()
        self.collectionView?.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar!.resignFirstResponder()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar!.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar!.setShowsCancelButton(false, animated: false)
    }
    func cancelSearching(){
        self.searchBar!.resignFirstResponder()
        self.searchBar!.text = ""
    }
    
    // MARK: prepareVC
    func prepareUI(){
        self.addSearchBar()
    }
    
    func addSearchBar(){
        if self.searchBar == nil{
            self.searchBar = UISearchBar(frame: CGRect(x: 20, y: 0, width: UIScreen.main.bounds.size.width-40, height: 44))
            self.searchBar!.tintColor = UIColor.white
            self.searchBar!.textColor = UIColor.white
            self.searchBar!.searchBarStyle = UISearchBarStyle.minimal
            self.searchBar!.delegate = self
            self.searchBar!.returnKeyType = .done
            self.searchBar!.placeholder = "Find gifs"
        }
        
        if !self.searchBar!.isDescendant(of: self.view){
            self.navigationItem.titleView = self.searchBar!
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if "showDetail" == segue.identifier {
            self.searchBar!.resignFirstResponder()
            let nextScene = segue.destination as! DetailViewController
            let indexPaths : NSArray = self.collectionView!.indexPathsForSelectedItems! as NSArray
            if let indexPath : NSIndexPath = indexPaths[0] as? NSIndexPath{
                if let name = self.dataSourceForSearchResult?[indexPath.row].largePict{
                    nextScene.imageString = name
                    nextScene.title = self.searchBar?.text
                }
            }
            
        }
    }
    


    func reloadData() {
        self.dataSourceForSearchResult = realm.objects(ImageItem.self).sorted(byKeyPath: "id")
        self.collectionView?.reloadSections(NSIndexSet(index: 0) as IndexSet)
        
        if self.dataSourceForSearchResult?.count != 0{
            nothingLabel.alpha = 0
        }
        else{
            nothingLabel.alpha = 1
        }
    }
    
    func getData(page: Int, searchString: String){
        lastLoadedPage = page
        
        self.queue.cancelAllOperations()
        if self.dataSourceForSearchResult?.count != 0{
            self.dataSourceForSearchResult = nil
        }
        
        let operation = BlockOperation(block: {
            
            self.translator.getJsonDataForPage(page: page, searchString: searchString) {
                gifs in
                self.translator.saveGifs(gifsDict: gifs as! [NSDictionary])
            }
        })
        operation.completionBlock = {
            //print("Operation completed")
        }
        queue.addOperation(operation)
    }

    func getItemsFromDatabase() -> Results<ImageItem> {
        do {
            let realm = try! Realm()
            return realm.objects(ImageItem.self)
        }
    }
}

extension UISearchBar {
    var textColor:UIColor? {
        get {
            if let textField = self.value(forKey: "searchField") as? UITextField  {
                return textField.textColor
            } else {
                return nil
            }
        }
        
        set (newValue) {
            if let textField = self.value(forKey: "searchField") as? UITextField  {
                textField.textColor = newValue
            }
        }
    }
}

extension UIColor{
    
        func getRandomColor() -> UIColor{
        
        let randomRed:CGFloat = CGFloat(drand48())
        
        let randomGreen:CGFloat = CGFloat(drand48())
        
        let randomBlue:CGFloat = CGFloat(drand48())
     
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
        
    }
}


extension UIColor {
    static var random: UIColor {
        srand48(Int(arc4random()))
        return UIColor(red: CGFloat(drand48()), green: CGFloat(drand48()), blue: CGFloat(drand48()), alpha: 1.0)
    }
}
